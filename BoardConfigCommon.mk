# inherit from the shinano-common
include device/sony/shinano-common/BoardConfigCommon.mk

# Power
TARGET_TAP_TO_WAKE_NODE := "/sys/devices/virtual/input/clearpad/wakeup_gesture"

# SELinux
BOARD_SEPOLICY_DIRS += $(DEVICE_PATH)/sepolicy

# Partition Information
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2671771648     # 2.548M
BOARD_USERDATAIMAGE_PARTITION_SIZE := 12253641728