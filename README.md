Copyright (C) 2019 The LineageOS Project

Device configuration for Sony Xperia Z3 Compact
=========================================

The Sony Xperia Z3 Compact Tablet (codename _"scorpion_windy"_) is a compact high-end tablet from Sony.

